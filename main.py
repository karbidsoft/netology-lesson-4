documents = [
        {"type": "passport", "number": "2207 876234", "name": "Василий Гупкин"},
        {"type": "invoice", "number": "11-2", "name": "Геннадий Покемонов"},
        {"type": "insurance", "number": "10006", "name": "Аристарх Павлов"},
        {"type": "passport", "number": "7766 832932", "name": "Девопс Линуксович"},
        {"type": "passport", "number": "8934 934903", "name": "Ансибле Паппетова"},
        {"type": "invoice", "number": "12-3", "name": "Центось Редхатова"},
        {"type": "invoice", "number": "16-5", "name": "Оперативка Новая"},
        {"type": "invoice", "number": "14-1", "name": "Василий Пупкин"},
        {"type": "insurance", "number": "10023", "name": "Андрей Апстартов"},
        {"type": "insurance", "number": "10012", "name": "Илья Системд"},
        {"type": "passport", "number": "5098 627353", "name": "Рут Админов"},
        {"type": "insurance", "number": "10016", "name": "Юзер Пузер"},
      ]

directories = {
        '1': ['2207 876234', '11-2'],
        '2': ['10006'],
        '3': ['7766 832932', '8934 934903'],
        '4': ['12-3', '16-5', '14-1'],
        '5': ['10023', '10012'],
        '6': ['5098 627353', '10016']
      }

def get_person_name(document_number):
  for document in documents:
    if document_number == document['number']:
      return document['name']
  return 'Человек не найден'

def show_all_documents():
  for document in documents:
    print('В базе есть документ: {type} {number} {name}' .format(**document))

def get_directory(document_number):
  for directory, items in directories.items():
    if document_number in items:
      return directory
  return 'На полках такого документа нет.'

def remove_document(document_number):
  for position, document in enumerate(documents):
    if document_number == document['number']:
      del documents[position]
      return '------------------ Документ удален! -------------------- \n'
  return '------------------- Документ не найден! --------------------\n'

def remove_document_from_directory(document_number):
  for directory, items in directories.items():
    if document_number in items:
      for position, item in enumerate(items):
        if item == document_number:
          del items[position]
          return 'Документ %s удален с полки %s' %(item, directory)
  return '----------------- Документ не найден, нечего удалять! -------------------\n'

def add_new_document(new_document):
  if len(directories) < int(new_document[3]):
    return 'Нет полки с номером %s, сперва добавьте эту полку' %(new_document[3])
  new_document_dic = dict()
  new_document_dic['type'] = new_document[0]
  new_document_dic['number'] = new_document[1]
  new_document_dic['name'] = new_document[2]
  documents.append(new_document_dic)
  for directory, items in directories.items():
    if new_document[3] == directory:
      items.append(new_document[1])
      return 'Новый документ %s добавлен на полку %s' %(new_document[1], new_document[3])


while True:
  print('--------------------------------------------------------------------------\n')
  print('Команды для работы с базой данных:\n')
  print('show - Показывает список всех документов на полке')
  print('person - Поиск человека по документу')
  print('dir - Поск полки по номеру документа')
  print('add - Добавление нового документа: тип(passport, invoice, insurance), номер, имя владельца, номер полки')
  print('remove - Удаление документа из списка документов')
  print('rmd - Удаление с полки документа')
  print('add_dir - Добавление новой полки')
  print('--------------------------------------------------------------------------\n')
  command = input('Введите команду:')
  if command.lower() == 'show':
    print('========================================================================')
    show_all_documents()
  elif command.lower() == 'person':
      document_number = input('Введите номер документа:')
      person_name = get_person_name(document_number)
      print('======================================================================')
      print('Человек с искомым документом:' , person_name)
  elif command.lower() == 'dir':
      document_number = input('Введите номер документа:')
      document_name = get_directory(document_number)
      print('======================================================================')
      print('Документ находится на полке:', document_name)
  elif command.lower() == 'add':
    new_document = input('Введите данные документа (тип(passport, invoice, insurance)), номер, имя владельца, номер полки):').split(', ')
    result = add_new_document(new_document)
    print(result)
  elif command.lower() == 'remove':
    document_number = input('Введите номер документа:')
    result = remove_document(document_number)
    print(result)
  elif command.lower() == 'add_dir':
    directories[str(len(directories)+1)] = []
    print('---------------- Новая полка %i добавлена! ------------------' % (len(directories)))
  elif command.lower() == 'rmd':
    document_number = input('Введите номер документа:')
    result = remove_document_from_directory(document_number)
    print(result)
